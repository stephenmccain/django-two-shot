from django.urls import path
from django.contrib.auth import views as auth_views


from receipts.views import (
    New_Expense_CategoryCreateView,
    New_ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
    ReceiptListView,
    New_AccountCreateView,

)


urlpatterns = [
     path("", ReceiptListView.as_view(), name="home"),
     path("categories/", ExpenseCategoryListView.as_view(),
          name="categories_list"),

     path("accounts/", AccountListView.as_view(),
          name="accounts_list"),

     path("categories/create/",
          New_Expense_CategoryCreateView.as_view(),
          name="new_expense_category_create"),

     path("create/", New_ReceiptCreateView.as_view(),
          name="new_receipt"),
   

     path("accounts/create/", New_AccountCreateView.as_view(),
          name="create_account"),
    ]